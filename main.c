#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <ctype.h>

typedef struct{
    char* name;
    off_t size;
    double percentage;
} FileInfo;

typedef struct{
    char* name;
    off_t size;
    double percentage;
} DirectoryInfo;

typedef struct{
    int value;
    char word[30];
} Compress_word;

typedef struct {
    int word_count;
    Compress_word words[100];
    char* file_path;
} Compressed_files;

Compressed_files compressed_files[1000];
int num_compressed_files = 0;

FileInfo* file_infos = NULL;
int num_files = 0;

DirectoryInfo* directory_infos = NULL;
int num_directories = 0;

char path[100];
char result_path[100];
char* included_extensions[100] = {0};
char* excluded_extensions[100] = {0};
int included_count = 0;
int excluded_count = 0;


void sort_files_by_size_ascending(FileInfo* file_infos, int num_files){
    int i, j;
    FileInfo temp;

    for(i = 0; i < num_files - 1; i++) {
        for(j = 0; j < num_files - i - 1; j++) {
            if(file_infos[j].size > file_infos[j+1].size) {
                temp = file_infos[j];
                file_infos[j] = file_infos[j+1];
                file_infos[j+1] = temp;
            }
        }
    }
}

void sort_files_by_size_descending(FileInfo* file_infos, int num_files){
    int i, j;
    FileInfo temp;

    for(i = 0; i < num_files - 1; i++) {
        for(j = 0; j < num_files - i - 1; j++) {
            if(file_infos[j].size < file_infos[j+1].size) {
                temp = file_infos[j];
                file_infos[j] = file_infos[j+1];
                file_infos[j+1] = temp;
            }
        }
    }
}

void add_extensions_to_list(const char* extensions, int* number) {
    char* extension;
    char* input = strdup(extensions);
    extension = strtok(input, ",");
    if(number == 1){
        while(extension != NULL){
            excluded_extensions[excluded_count] = strdup(extension);
            excluded_count++;
            extension = strtok(NULL, ",");
        }
    }
    else if(number == 2){
        while(extension != NULL) {
            included_extensions[included_count] = strdup(extension);
            included_count++;
            extension = strtok(NULL, ",");
        }
    }
    free(input);
}

int check_extension(char** extensions, int count, char* extension) {
    for (int i = 0; i < count; i++) {
        if (strcmp(extensions[i], extension) == 0) {
            return 1;
        }
    }
    return 0;
}

int check_directory(const char* path){
    struct stat path_stat;
    stat(path, &path_stat);
    return S_ISDIR(path_stat.st_mode);
}

off_t calculate_directory_size(const char* dir_path) {
    DIR* dir = opendir(dir_path);
    if (dir == NULL) {
        printf("Greshka pri otvarqne na direktoriqta %s\n", dir_path);
        return 0;
    }

    long long total_size = 0;
    struct dirent* dp;
    while ((dp = readdir(dir)) != NULL) {
        if (strcmp(dp->d_name, ".") == 0 || strcmp(dp->d_name, "..") == 0) {
            continue;
        }

        char file_path[4000];
        sprintf(file_path, "%s/%s", dir_path, dp->d_name);

        struct stat st;
        if (stat(file_path, &st) == 0) {
            if (S_ISREG(st.st_mode)) {
                total_size += st.st_size;
            } else if (S_ISDIR(st.st_mode)) {
                total_size += calculate_directory_size(file_path);
            }
        }
    }

    closedir(dir);
    return total_size;
}

void add_directory_to_list(DirectoryInfo** dir_infos_ptr, int* num_dirs_ptr, const char* dir_path, int* total_size_ptr) {
    off_t dir_size = calculate_directory_size(dir_path);
    struct stat st;
    if (stat(dir_path, &st) == 0){
        DirectoryInfo dir_info = {
            .name = strdup(dir_path),
            .size = dir_size,
            .percentage = 0.0
        };

        *total_size_ptr += dir_size;

        int path_length = strlen(dir_path);
        if (path_length > 30) {
            strncpy(&dir_info.name[path_length - 3], "...", 3);
        }

        *dir_infos_ptr = realloc(*dir_infos_ptr, (*num_dirs_ptr + 1) * sizeof(DirectoryInfo));

        (*dir_infos_ptr)[*num_dirs_ptr] = dir_info;
        (*num_dirs_ptr)++;
    }

    DIR* dir = opendir(dir_path);
    if(dir == NULL) {
        printf("Greshka pri otvarqne na direktoriqta %s\n", dir_path);
        return;
    }

    struct dirent* dp;
    while((dp = readdir(dir)) != NULL) {
        if(strcmp(dp->d_name, ".") == 0 || strcmp(dp->d_name, "..") == 0) {
            continue;
        }

        char file_path[4000];
        sprintf(file_path, "%s/%s", dir_path, dp->d_name);

        if(check_directory(file_path)){
            add_directory_to_list(dir_infos_ptr, num_dirs_ptr, file_path, total_size_ptr);
        }
    }
    closedir(dir);

    for (int i = 0; i < *num_dirs_ptr; i++) {
        (*dir_infos_ptr)[i].percentage = ((double)(*dir_infos_ptr)[i].size / (double)(*total_size_ptr)) * 100.0;
    }
}

void add_file_to_list(FileInfo** file_infos_ptr, int* num_files_ptr, const char* file_path, int* total_size_ptr, char path_fixation){
    struct stat st;
    if(stat(file_path, &st) == 0){
        char* extension = strrchr(file_path, '.');
        if(extension != NULL && check_extension(excluded_extensions, excluded_count, extension)){
            return;
        }

        FileInfo file_info = {
            .name = strdup(file_path),
            .size = st.st_size,
            .percentage = 0.0
        };

        *total_size_ptr += st.st_size;

        if(path_fixation == 'Y'){
            int path_length = strlen(file_path);
            if(path_length > 30){
                strncpy(&file_info.name[path_length - 3], "...", 3);
            }
        }

        *file_infos_ptr = realloc(*file_infos_ptr, (*num_files_ptr + 1) * sizeof(FileInfo));

        (*file_infos_ptr)[*num_files_ptr] = file_info;
        (*num_files_ptr)++;
    }

    for(int i = 0; i < *num_files_ptr; i++){
        (*file_infos_ptr)[i].percentage = (double)(*file_infos_ptr)[i].size / (double)(*total_size_ptr) * 100.0;
    }
}

void add_compressed_to_list(const Compress_word* words, int num_words, const char* file_path) {
    if (num_compressed_files < 1000){
        Compressed_files compressed_file = {
            .word_count = num_words,
            .file_path = strdup(file_path)
        };
        for (int i = 0; i < num_words; i++) {
            compressed_file.words[i] = words[i];
        }
        compressed_files[num_compressed_files] = compressed_file;
        num_compressed_files++;
    }
}

void get_directory_contents(const char* dir_path, void** infos_ptr, int* num_infos_ptr, int* total_size_ptr, char add_type, char path_fixation){
    DIR* dir = opendir(dir_path);
    if(dir == NULL) {
        printf("Greshka pri otvarqne na direkotirqta %s\n", dir_path);
    }

    struct dirent* dp;
    while((dp = readdir(dir)) != NULL) {
        if(strcmp(dp->d_name, ".") == 0 || strcmp(dp->d_name, "..") == 0) {
            continue;
        }

        char file_path[1000];
        sprintf(file_path, "%s/%s", dir_path, dp->d_name);

        if(check_directory(file_path)){
            if(add_type == 'd') {
                add_directory_to_list((DirectoryInfo**)infos_ptr, num_infos_ptr, file_path, total_size_ptr);
            }
            else {
                get_directory_contents(file_path, infos_ptr, num_infos_ptr, total_size_ptr, add_type, path_fixation);
            }
        }
        else{
            if(add_type == 'd') {
                continue;
            }
            else {
                add_file_to_list(infos_ptr, num_infos_ptr, file_path, total_size_ptr, path_fixation);
            }
        }
    }
    closedir(dir);
}

void print_files_table(const char* path, const char* result_path, char sorting_type, char printing_type, char system_type){
    FileInfo* file_infos = NULL;
    int num_files = 0;
    long long total_size = 0;

    get_directory_contents(path, &file_infos, &num_files, &total_size, system_type, 'Y');

    if (sorting_type == 'U') {
        sort_files_by_size_ascending(file_infos, num_files);
    }
    else if (sorting_type == 'D') {
        sort_files_by_size_descending(file_infos, num_files);
    }

    int max_len = 0;
    for (int i = 0; i < num_files; i++) {
        int len = strlen(file_infos[i].name);
        if (len > max_len) {
            max_len = len;
        }
    }

    FILE* result_file = fopen(result_path, "w");
    if(result_file == NULL){
        printf("Nqma izhoden fail!\n");
    }
    else if(printing_type != 'b'){
        fprintf(result_file, "\nPuteka do faila");
        for(int i = 0; i < max_len - 15; i++){
            fprintf(result_file, " ");
        }
        fprintf(result_file, "|Razmer kb     | %%       |\n");

        for(int i = 0; i < max_len; i++){
            fprintf(result_file, "-");
        }
        fprintf(result_file, "+");
        for(int i = 0; i < 14; i++){
            fprintf(result_file, "-");
        }
        fprintf(result_file, "+");
        for(int i = 0; i < 9; i++){
            fprintf(result_file, "-");
        }
        fprintf(result_file, "|\n");

        for(int i = 0; i < num_files; i++){
            fprintf(result_file, "%s", file_infos[i].name);
            for(int j = 0; j < max_len - strlen(file_infos[i].name); j++){
                fprintf(result_file, " ");
            }
            fprintf(result_file, "|%10lld kB |%7.2f%% |\n", file_infos[i].size / 1024, file_infos[i].percentage);
        }

        for(int i = 0; i < max_len+25; i++){
            fprintf(result_file, "-");
        }
        fprintf(result_file, "+\n");
        fclose(result_file);
    }

    printf("\nPuteka do faila");
    for(int i = 0; i < max_len - 15; i++) {
        printf(" ");
    }
    printf("|Razmer kB     | %%       |\n");

    for(int i = 0; i < max_len; i++) {
        printf("-");
    }
    printf("+");
    for(int i = 0; i < 14; i++){
        printf("-");
    }
    printf("+");
    for(int i = 0; i < 9; i++){
        printf("-");
    }
    printf("|\n");

    for(int i = 0; i < num_files; i++) {
        printf("%s", file_infos[i].name);
        for(int j = 0; j < max_len - strlen(file_infos[i].name); j++) {
            printf(" ");
        }
        printf("|%10lld    |%7.2f%% |\n", file_infos[i].size / 1024, file_infos[i].percentage);
    }

    for(int i = 0; i < max_len+25; i++){
        printf("-");
    }
    printf("+\n");
    free(file_infos);
}

void print_files_diagram(const char* path, const char* result_path, char sorting_type, char printing_type, char system_type){
    FileInfo* file_infos = NULL;
    int num_files = 0;
    long long total_size = 0;

    get_directory_contents(path, &file_infos, &num_files, &total_size, system_type, 'Y');

    if (sorting_type == 'U') {
        sort_files_by_size_ascending(file_infos, num_files);
    }
    else if (sorting_type == 'D') {
        sort_files_by_size_descending(file_infos, num_files);
    }

    int max_len = 0;
    for (int i = 0; i < num_files; i++) {
        int len = strlen(file_infos[i].name);
        if (len > max_len) {
            max_len = len;
        }
    }
    double biggest_percentage = 0.0;
    for(int i = 0; i < num_files; i++){
        if(file_infos[i].percentage > biggest_percentage){
            biggest_percentage = file_infos[i].percentage;
        }
    }

    FILE* result_file = fopen(result_path, "w");
    if(result_file == NULL){
        printf("Nqma izhoden fail!\n");
    }
    else if(printing_type == 'b'){
        fprintf(result_file, "\nPuteka do faila");
        for(int i = 0; i < max_len - 15; i++){
            fprintf(result_file, " ");
        }
        fprintf(result_file, "|Razmer kb     | %%       |\n");

        for(int i = 0; i < max_len; i++){
            fprintf(result_file, "-");
        }
        fprintf(result_file, "+");
        for(int i = 0; i < 14; i++){
            fprintf(result_file, "-");
        }
        fprintf(result_file, "+");
        for(int i = 0; i < 9; i++){
            fprintf(result_file, "-");
        }
        fprintf(result_file, "|\n");

        for(int i = 0; i < num_files; i++){
            fprintf(result_file, "%s", file_infos[i].name);
            for(int j = 0; j < max_len - strlen(file_infos[i].name); j++){
                fprintf(result_file, " ");
            }
            fprintf(result_file, "|%10lld kB |%7.2f%% |\n", file_infos[i].size / 1024, file_infos[i].percentage);
        }

        for(int i = 0; i < max_len+25; i++){
            fprintf(result_file, "-");
        }
        fprintf(result_file, "+\n");
        fprintf(result_file, "\nFile Path");
        for(int i = 0; i < max_len-9; i++){
            fprintf(result_file, " ");
        }
        fprintf(result_file, "|");

        for(double i = 0.0; i < (biggest_percentage/2)-7; i++){
            fprintf(result_file, " ");
        }
        fprintf(result_file, "Memory usage %%");
        for(double i = 0.0; i < (biggest_percentage/2)-7; i++){
            fprintf(result_file, " ");
        }
        fprintf(result_file, "|\n");

        for(int i = 0; i < max_len; i++){
            fprintf(result_file, "-");
        }
        fprintf(result_file, "+");
        for(double i = 0.0; i < biggest_percentage+1; i++){
            fprintf(result_file, "-");
        }
        fprintf(result_file, "+\n");

        for(int i = 0; i < num_files; i++){
            fprintf(result_file, "%s", file_infos[i].name);
            for(int j = 0; j < max_len - strlen(file_infos[i].name); j++) {
                fprintf(result_file, " ");
            }
            fprintf(result_file, "|");
            for(int j = 0; j < biggest_percentage; j++){
                if(j < (int)(file_infos[i].percentage)){
                    fprintf(result_file, "=");
                }
                else{
                    fprintf(result_file, " ");
                }
            }
            fprintf(result_file, " |\n");
        }
        for(int i = 0; i < max_len; i++){
            fprintf(result_file, " ");
        }
        fprintf(result_file, "+");
        for(int i = 0; i < biggest_percentage+1; i++){
            fprintf(result_file, "-");
        }
        fprintf(result_file, "+\n");

        for(int i = 0; i < max_len; i++){
            fprintf(result_file, " ");
        }
        for(int i = 0; i < biggest_percentage; i+=4){
            fprintf(result_file, "%d   ", i);
        }

        fclose(result_file);
    }
    else{
        fprintf(result_file, "\nFile Path");
        for(int i = 0; i < max_len-9; i++){
            fprintf(result_file, " ");
        }
        fprintf(result_file, "|");

        for(double i = 0.0; i < (biggest_percentage/2)-7; i++){
            fprintf(result_file, " ");
        }
        fprintf(result_file, "Memory usage %%");
        for(double i = 0.0; i < (biggest_percentage/2)-7; i++){
            fprintf(result_file, " ");
        }
        fprintf(result_file, "|\n");

        for(int i = 0; i < max_len; i++){
            fprintf(result_file, "-");
        }
        fprintf(result_file, "+");
        for(double i = 0.0; i < biggest_percentage+1; i++){
            fprintf(result_file, "-");
        }
        fprintf(result_file, "+\n");

        for(int i = 0; i < num_files; i++){
            fprintf(result_file, "%s", file_infos[i].name);
            for(int j = 0; j < max_len - strlen(file_infos[i].name); j++) {
                fprintf(result_file, " ");
            }
            fprintf(result_file, "|");
            for(int j = 0; j < biggest_percentage; j++){
                if(j < (int)(file_infos[i].percentage)){
                    fprintf(result_file, "=");
                }
                else{
                    fprintf(result_file, " ");
                }
            }
            fprintf(result_file, " |\n");
        }
        for(int i = 0; i < max_len; i++){
            fprintf(result_file, " ");
        }
        fprintf(result_file, "+");
        for(int i = 0; i < biggest_percentage+1; i++){
            fprintf(result_file, "-");
        }
        fprintf(result_file, "+\n");

        for(int i = 0; i < max_len; i++){
            fprintf(result_file, " ");
        }
        for(int i = 0; i < biggest_percentage; i+=4){
            fprintf(result_file, "%d   ", i);
        }

        fclose(result_file);
    }

    printf("\nFile Path");
    for(int i = 0; i < max_len-9; i++){
        printf(" ");
    }
    printf("|");

    for(double i = 0.0; i < (biggest_percentage/2)-7; i++){
        printf(" ");
    }
    printf("Memory usage %%");
    for(double i = 0.0; i < (biggest_percentage/2)-7; i++){
        printf(" ");
    }
    printf("|\n");

    for(int i = 0; i < max_len; i++) {
        printf("-");
    }
    printf("+");
    for(double i = 0.0; i < biggest_percentage+1; i++){
        printf("-");
    }
    printf("+\n");

    for(int i = 0; i < num_files; i++){
        printf("%s", file_infos[i].name);
        for(int j = 0; j < max_len - strlen(file_infos[i].name); j++) {
            printf(" ");
        }
        printf("|");
        for(int j = 0; j < biggest_percentage; j++){
            if(j < (int)(file_infos[i].percentage)){
                printf("=");
            }
            else{
                printf(" ");
            }
        }
        printf(" |\n");
    }

    for(int i = 0; i < max_len; i++){
        printf(" ");
    }
    printf("+");
    for(int i = 0; i < biggest_percentage+1; i++){
        printf("-");
    }
    printf("+\n");

    for(int i = 0; i < max_len; i++){
        printf(" ");
    }
    for(int i = 0; i < biggest_percentage; i+=4){
        printf("%d   ", i);
    }

    free(file_infos);
}

void compressing(const char* file_path, char deletion){

    FILE *file = fopen(file_path, "rb");
    if(file == NULL){
        printf("Greshka pri otvarqne na fail %s!\n", file_path);
        return;
    }

    Compress_word words[50];
    int num_words = 2;
    words[0].value = 0;
    strcpy(words[0].word, " ");
    words[1].value = 1;
    strcpy(words[1].word, ",");

    char word[30];
    while(fscanf(file, "%s", word) != EOF){
        int i;
        for(int j = 0; j < strlen(word); j++){
            if(word[j] == ','){
                word[j] = '\0';
            }
        }
        for(i = 0; i < num_words; i++){
            if(strcmp(words[i].word, word) == 0){
                break;
            }
        }

        if(i == num_words){
            strcpy(words[i].word, word);
            words[i].value = num_words;
            num_words++;
        }
    }
    fclose(file);

    char result_file_path[100];
    sprintf(result_file_path, "%s.cca", file_path);
    FILE *result_file = fopen(result_file_path, "wb");

    if(result_file == NULL){
        printf("Greshka pri otvarqne na kraen fail %s!\n", result_file_path);
        return;
    }

    /*for(int i = 0; i < num_words; i++){
        fprintf(result_file, "%d-%s\n", words[i].value, words[i].word);
    }*/

    file = fopen(file_path, "rb");
    if(file == NULL){
        printf("Greshka pri otvarqne na fail %s!\n", file_path);
        return;
    }

    char whitespace;
    char print_comma;
    while(fscanf(file, "%s", word) != EOF){
        int current_value;

        for(int j = 0; j < strlen(word); j++){
            if(word[j] == ','){
                word[j] = '\0';
                print_comma = 'Y';
            }
            else{
                print_comma = 'N';
            }
        }

        for(int i = 0; i < num_words; i++){
            if(strcmp(words[i].word, word) == 0){
                fprintf(result_file, "%d", words[i].value);
                fprintf(result_file, "A");
            }
        }

        if(print_comma == 'Y'){
            fprintf(result_file, "%d", words[1].value);
            fprintf(result_file, "A");
        }

        whitespace = fgetc(file);
        if(isspace(whitespace)){
            fprintf(result_file, "%d", words[0].value);
            fprintf(result_file, "A");
        }
        else{
            ungetc(whitespace, file);
        }
    }
    add_compressed_to_list(words, num_words, result_file_path);

    fclose(file);

    if(deletion == 'Y'){
        remove(file_path);
    }

    fclose(result_file);

}

void decompressing(const char* file_path, char deletion){
    FILE* file = fopen(file_path, "rb");
    if(file == NULL){
        printf("Greshka pri otvarqne na fail %s!\n", file_path);
        return;
    }

    char result_file_path[100];
    sprintf(result_file_path, "%sDC.txt", file_path);
    FILE *result_file = fopen(result_file_path, "wb");
    if(result_file == NULL){
        printf("Greshka pri otvarqne na fail %s!\n", result_file_path);
    }

    char code[500];
    fgets(code, 500, file);
    char *token;
    int converter;

    token = strtok(code, "A");
    while(token != NULL){
        converter = strtol(token, NULL, 10);
        if(converter == 0){
            fprintf(result_file, " ");
        }
        else{
            for(int i = 0; i < num_compressed_files; i++){
                if(strcmp(file_path, compressed_files[i].file_path) == 0){
                    for(int j = 0; j < compressed_files[i].word_count; j++){
                        if(converter == compressed_files[i].words[j].value){
                            fprintf(result_file, "%s", compressed_files[i].words[j].word);
                        }
                    }
                }
            }
        }
        token = strtok(NULL, "A");
    }

    fclose(file);
    fclose(result_file);

    if(deletion == 'Y'){
        remove(file_path);
    }
}

int main()
{

    char input[100];
    char *command, *key;
    char is_compressing;
    int compressing_command = 0;
    char is_decompressing[4];
    int decompressing_command = 0;
    int solo_decompressing_command = 0;

    while(1){
        printf("\nSpisuk s Komandi:\n");
        printf("(Zaaduljitelno) -D \"Put do faila s koito shte rabotim\"\n");
        printf("(Opcionalno) -I \"Put do izhoden fail v koito da se zapisva rezultat ot analiza\"\n");
        printf("(Opcionalno) -pf \"Razshireniq na failove, koito iskame da prisustvat\"\n");
        printf("(Opcionalno) -nf \"Razshireniq na failove, koito ne iskame da prisustvat\"\n");
        printf("(Printirane) -AF \"Printirane na dannite za failovete v zavisimost ot parametrite (t, d, b, U, D)\"\n");
        printf("(Printirane) -AD \"Printirane na dannite za direktoriite v zavisimost ot parametrite (t, d, b, U, D)\"\n");
        printf("Napishte \"Exit\" za izlizane ot tova menu!\n");

        fgets(input, sizeof(input), stdin);
        input[strcspn(input, "\n")] = '\0';

        command = strtok(input, " ");
        key = strtok(NULL, " ");

        if(strcmp(command, "Exit") == 0){
            break;
        }

        else if(strcmp(command, "-D") == 0){
            strcpy(path, key);
        }

        else if(strcmp(command, "-I") == 0){
            strcpy(result_path, key);
        }

        else if(strcmp(command, "-pf") == 0){
            add_extensions_to_list(key, 2);
        }

        else if(strcmp(command, "-nf") == 0){
            add_extensions_to_list(key, 1);
        }

        else if(strcmp(command, "-AF") == 0){
            char type = key[0];
            char sort_type = key[1];

            if(sort_type == 'U'){
                if(type == 't'){
                    print_files_table(path, result_path, 'U', 't', 'f');
                }
                else if(type == 'd'){
                    print_files_diagram(path, result_path, 'U', 'd', 'f');
                }
                else if(type == 'b'){
                    print_files_table(path, result_path, 'U', 'b', 'f');
                    print_files_diagram(path,  result_path, 'U', 'b', 'f');
                }
                else{
                    printf("Greshna opciq %s pri -AF komanda!", type);
                }
            }
            else if(sort_type == 'D'){
                if(type == 't'){
                    print_files_table(path, result_path, 'D', 't', 'f');
                }
                else if(type == 'd'){
                    print_files_diagram(path, result_path, 'D', 'd', 'f');
                }
                else if(type == 'b'){
                    print_files_table(path, result_path, 'D', 'b', 'f');
                    print_files_diagram(path,  result_path, 'D', 'b', 'f');
                }
                else{
                    printf("Greshna opciq %s pri -AF komanda!", type);
                }
            }
            else{
                printf("Greshna opciq %s pri -AF komanda!", sort_type);
            }
        }

        else if(strcmp(command, "-AD") == 0){
            char type = key[0];
            char sort_type = key[1];

            if(sort_type == 'U'){
                if(type == 't'){
                    print_files_table(path, result_path, 'U', 't', 'd');
                }
                else if(type == 'd'){
                    print_files_diagram(path, result_path, 'U', 'd', 'd');
                }
                else if(type == 'b'){
                    print_files_table(path, result_path, 'U', 'b', 'd');
                    print_files_diagram(path, result_path, 'U', 'b', 'd');
                }
                else{
                    printf("Greshna opciq %s pri -AD komanda!", type);
                }
            }
            else if(sort_type == 'D'){
                if(type == 't'){
                    print_files_table(path, result_path, 'D', 't', 'd');
                }
                else if(type == 'd'){
                    print_files_diagram(path, result_path, 'D', 'd', 'd');
                }
                else if(type == 'b'){
                    print_files_table(path, result_path, 'D', 'b', 'd');
                    print_files_diagram(path, result_path, 'D', 'b', 'd');
                }
                else{
                    printf("Greshna opciq %s pri -AD komanda!", type);
                }
            }
            else{
                printf("Greshna opciq %s pri -AD komanda!", sort_type);
            }
        }
        else{
            printf("Greshna komanda %s!", command);
        }

    }
    printf("Shte jelaete li da kompresirate failove ot nai-golqm kum nai-maluk (raboti samo sus .txt failove)?(Y/N)\n");
    scanf("%c", &is_compressing);
    if(is_compressing == 'Y'){
        long long total_size = 0;

        DIR* dir = opendir(path);
        if(dir == NULL) {
            printf("Greshka pri otvarqne na direkotirqta %s\n", path);
            return;
        }

        get_directory_contents(path, &file_infos, &num_files, &total_size, 'f', 'N');
        sort_files_by_size_descending(file_infos, num_files);
        for(int i = 0; i < num_files; i++){
            printf("\nTekusht fail: %s\n", file_infos[i].name);
            printf("Spisuk s komandi:\n");
            printf("(1) Kompresirane na segashniq fail\n");
            printf("(2) Kompresirane i iztrivane na sefashniq fail\n");
            printf("(3) Preskachane na segashniq fail\n");
            printf("(4) Prikliuchvane na kompresaciq\n");
            printf("Napishete opciq ot 1 do 4: ");
            scanf("%d", &compressing_command);

            if(compressing_command == 1){
                compressing(file_infos[i].name, 'N');
            }
            else if(compressing_command == 2){
                compressing(file_infos[i].name, 'Y');
            }
            else if(compressing_command == 3){
                continue;
            }
            else if(compressing_command == 4){
                break;
            }
            else{
                printf("Nqma opciq %d!\n", compressing_command);
            }
        }
    }

    printf("Shte jelaete li dekompresaciq (raboti samo na predishno kompresiranite failove)? (Komanda -DC)\n");
    scanf("%s", is_decompressing);
    if(strcmp(is_decompressing, "-DC") == 0){
        if(num_compressed_files != 0){
            printf("Kompresirani failove:\n");
            for(int i = 0; i < num_compressed_files; i++){
                printf("%s\n", compressed_files[i].file_path);
            }
            for(int i = 0; i < num_compressed_files; i++){
                printf("Menu za dekompresaciq:\n");
                printf("(1) Dekompresaciq na vsichki failove\n");
                printf("(2) Dekompresaciq na vsichki failove i iztrivane\n");
                printf("(3) Dekompresaciq na fail pootdelno\n");
                printf("Napishete opciq ot 1 do 3: ");
                scanf("%d", &decompressing_command);

                if(decompressing_command == 1){
                    for(int j = 0; j < num_compressed_files; j++){
                        decompressing(compressed_files[j].file_path, 'N');
                    }
                    break;
                }
                else if(decompressing_command == 2){
                    for(int j = 0; j < num_compressed_files; j++){
                        decompressing(compressed_files[j].file_path, 'Y');
                    }
                    break;
                }
                else if(decompressing_command == 3){
                    printf("\nDekompresaciq na vseki edin fail pootdelno\n");
                    for(int j = 0; j < num_compressed_files; j++){
                        printf("\nTekusht fail: %s\n", compressed_files[j].file_path);
                        printf("Menu za dekompresaciq:\n");
                        printf("(1) Dekompresaciq na segashniq fail\n");
                        printf("(2) Dekompresaciq na segashniq fail s iztrivane\n");
                        printf("(3) Preskachane na segashniq fail\n");
                        printf("Napishete opciq ot 1 do 3: ");
                        scanf("%d", &solo_decompressing_command);

                        if(solo_decompressing_command == 1){
                            decompressing(compressed_files[j].file_path, 'N');
                        }
                        else if(solo_decompressing_command == 2){
                            decompressing(compressed_files[j].file_path, 'Y');
                        }
                        else if(solo_decompressing_command == 3){
                            continue;
                        }
                        else{
                            printf("Greshna opciq %d!", solo_decompressing_command);
                        }
                    }
                    break;
                }
                else{
                    printf("Greshna opciq %d!", decompressing_command);
                }
            }
        }
        else{
            printf("Nqma kompresirani failove!\n");
        }
    }

    printf("\nDovijdane!\n");

    return 0;
}
